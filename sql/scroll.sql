-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 15, 2018 at 04:14 PM
-- Server version: 5.6.38
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(115) NOT NULL,
  `subtitle` varchar(115) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `category` varchar(115) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `subtitle`, `description`, `image`, `category`) VALUES
(1, 'title1', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product1.jpg', 'cat1'),
(2, 'title2', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product2.jpg', 'cat2'),
(3, 'title3', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product3.jpg', 'cat3'),
(4, 'title4', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product4.jpg', 'cat1'),
(5, 'title5', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product5.jpg', 'cat1'),
(6, 'title6', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product6.jpg', 'cat2'),
(7, 'title7', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product7.jpg', 'cat2'),
(8, 'title8', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product8.jpg', 'cat2'),
(9, 'title9', 'subtitle', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed vehicula ipsum. Sed elementum, metus vitae commodo euismod, mi erat accumsan dui, et maximus mi ex vel orci. Vivamus sed diam sed nisl volutpat gravida efficitur tempor orci. Nulla facilisi. Ut viverra fermentum rhoncus. Cras ipsum tellus, molestie ac justo et, lacinia pharetra purus. Nam rhoncus mi interdum urna laoreet, a egestas sapien pellentesque. Sed vitae ligula pretium, molestie nulla nec, aliquam massa. Nullam vitae nulla lacinia, sollicitudin massa ut, aliquet tortor. Maecenas porttitor lobortis ipsum, vestibulum facilisis lacus semper at. Phasellus id neque eros. In sit amet quam lobortis ex condimentum dignissim sed ultrices est. Integer ut ultricies sem, non euismod enim.</p>\r\n\r\n<p>Praesent sit amet leo vel lorem malesuada feugiat. Nullam sed euismod risus. Curabitur accumsan ante eget libero vulputate elementum. Aenean et interdum turpis. Vivamus orci sem, egestas at dolor ut, venenatis tempus ipsum. Etiam id neque iaculis, dignissim tortor sollicitudin, facilisis ex. Sed quis ipsum sed odio suscipit pharetra. Nulla pharetra risus eu risus volutpat, nec ultrices libero tristique. Proin iaculis euismod lacinia. </p>', 'images/product9.jpg', 'cat2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
