<?php

include 'controller.php';
$main = new Main();

if (isset($_GET['category']) and $_GET['category'] != 'all') {
    $first_products = $main->getCategory($_GET['category']); //loading the next batch of products
} elseif (isset($_GET['category']) and $_GET['category'] == 'all') {
    $first_products = $main->showProducts(); //loading the next batch of products
} else {
    $first_products = $main->moreProducts($_GET['showmore'], $_GET['lastid']); //loading the next batch of products
}
$json = include 'views/product.php';
