<?php

class Products
{
    //edit the database settings below.
    protected $host = '127.0.0.1';
    protected $user = 'root';
    protected $password = 'root';
    protected $database = 'scroll';
    public function __construct()
    {
        $this->mysqli = new mysqli($this->host, $this->user, $this->password, $this->database);
        if ($this->mysqli->connect_errno) {
            echo "Error: " . $this->mysqli->connect_error . "\n";
            exit;
        }
    }

    public function getLastId()
    {
        $sql = "SELECT id FROM products ORDER BY id DESC LIMIT 1";
        $result = $this->mysqli->query($sql);
        return $result->fetch_array()[0];
    }

    public function showProducts()
    {
        $sql = "SELECT * FROM products ORDER BY id ASC LIMIT 4";
        $result = $this->mysqli->query($sql);
        while ($row = $result->fetch_assoc()) {
            $products[] = $row;
        }
        return $products;
    }

    public function getProducts($nr, $lastid)
    {
        $sql = "SELECT * FROM products WHERE id > $lastid ORDER BY id ASC LIMIT $nr";
        $result = $this->mysqli->query($sql);
        while ($row = $result->fetch_assoc()) {
            $products[] = $row;
        }
        return $products;
    }

    public function getCategory($cat)
    {
        $sql = "SELECT * FROM products WHERE category = '$cat'";
        $result = $this->mysqli->query($sql);
        while ($row = $result->fetch_assoc()) {
            $products[] = $row;
        }
        return $products;
    }
}
