<?php
// main controller. it will call a model for DB interaction, and present data in a view file.
include 'controller.php';
$main = new Main;

$first_products = $main->showProducts(); //loading the first few products
$lastid = $main->getLastId(); //getting the last id so we can reset when we get to that and start from the first id.

include 'views/layout.php';
