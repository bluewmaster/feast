# README #


### Feast developer test ###

* Simple script that loads products from a database into an infinite scroller.
* Simple category filtering.
* Bootstrap 4 inside

### Set up and test it ###

* Download the files into your local server.
* Create a database and import the /sql/scroll.sql file
* Edit the models/productModel.php file to update the database info
* Open it in your browser: http://localhost/products/
* Click the "load more" button to load more products
* After the button is clicked, more products are being loaded on scroll down
* You can select a category on top and the products of that category will be loaded.
