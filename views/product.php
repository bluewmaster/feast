      <?php foreach ($first_products['products'] as $key => $product) {
    ?>
        <div class="card bg-dark text-white post-id" id="<?php echo $product['id']; ?>">
          <div class="overlay"></div>
          <img class="card-img" src="<?php echo $product['image']; ?>" alt="Card image cap">
          <div class="card-img-overlay">
            <h5 class="card-title"><?php echo $product['title']; ?></h5>
            <div class="card-text"><?php
            $desc=strpos($product['description'], ' ', 200);
    echo substr($product['description'], 0, $desc); ?></div>
          </div>
        </div>
      <?php
} ?>
