<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Products</title>
    <link rel="stylesheet" href="views/css/bootstrap.min.css" >
    <link rel="stylesheet" href="views/css/products.css" >
  </head>
  <body>

    <div class="container">
      <div class="row">
        <header>
          <div class="page-header">
            <h1>Infinite scrolling with products demo</h1>
          </div>
        </header>
      </div>

      <div class="row">
        <div class="col-lg-9">
              <p>Use the dropdown on the right side to view products from a certain category.</p>
              <p>Click the Load More button to load more products.</p>
              <p>After the Load more button is clicked, scroll down to load more products.</p>
        </div>
        <div class="col-lg-3 text-right">
          <form>
            <div class="form-group">
              <select class="form-control" name="category">
                <option value="all">Select category</option>
                <option value="cat1">Category 1</option>
                <option value="cat2">Category 2</option>
                <option value="cat3">Category 3</option>
              </select>
            </div>
          </form>
        </div>
      </div>

        <div class="row">

          <div class="card-columns" id="post-data">
            <?php include 'views/product.php'; ?>
          </div>

        </div>

        <div class="ajax-load text-center" style="display:none">
          <div class="loader"></div>
        </div>

        <div class="row d-block text-center">
          <a href="#" id="loadMore" class="btn btn-primary">load more</a>
        </div>

      </div>

  </body>

  <script src="views/js/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="views/js/bootstrap.min.js"></script>
  <script src="//unpkg.com/jscroll/dist/jquery.jscroll.min.js"></script>

  <script type="text/javascript">
       $('#loadMore').click(function(){
         jQuery('#loadMore').hide();
         //console.log('load more clicked');
         var last_id = jQuery(".post-id:last").attr("id");
         loadMoreData(last_id);

           jQuery(window).scroll(function() {
              if(jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height()) {
                  var last_id = jQuery(".post-id:last").attr("id");
                  if(last_id == <?php echo $lastid; ?>) {
                    last_id = 0;
                  }
                  //console.log(last_id);
                  loadMoreData(last_id);
              }
            });
      });

      function loadMoreData(last_id){
        jQuery.ajax({
                  url: 'loadmore.php?showmore=3&lastid=' + last_id, //decide here how many new prods to get.
                  type: "get",
                  beforeSend: function()

{

    $('.ajax-load').show();

}
              })
              .done(function(data) {
                  jQuery("#post-data").append(data);
              })
              .fail(function(jqXHR, ajaxOptions, thrownError) {
                  console.log(thrownError);
              });
      }

      $('select').on('change', function() {
          loadCategory(this.value);
          if(this.value != 'all') {
            jQuery('#loadMore').hide();

          } else {
            jQuery('#loadMore').show();
          }
          jQuery('.ajax-load').hide();
})
      function loadCategory(category){
        jQuery.ajax({
                  url: 'loadmore.php?category=' + category, //in this case we load all the products from the category
                  type: "get",
                  beforeSend: function()

{

    $('.ajax-load').show();

}
              })
              .done(function(data) {
                  jQuery("#post-data").empty().append(data);
              })
              .fail(function(jqXHR, ajaxOptions, thrownError) {
                  console.log(thrownError);
              });
      }
  </script>
</html>
