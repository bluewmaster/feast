<?php

class Main
{
    public function __construct()
    {
        include 'models/productModel.php';
        $this->db = new Products;
    }

    public function showProducts()
    {
        $products = $this->db->showProducts();
        $lastid = max(array_column($products, 'id'));
        return array('products' => $products,
                 'lastid' => $lastid);
    }

    public function getCategory($cat)
    {
        $products = $this->db->getCategory($cat);
        $lastid = max(array_column($products, 'id'));
        return array('products' => $products,
                 'lastid' => $lastid);
    }

    public function moreProducts($nr, $lastid)
    {
        $products = $this->db->getProducts($nr, $lastid);
        $lastid = max(array_column($products, 'id'));
        return array('products' => $products,
                 'lastid' => $lastid);
    }

    public function getLastId()
    {
        $lastid = $this->db->getLastId();
        return $lastid;
    }
}
